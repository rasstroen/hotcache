<?php

namespace bee;

abstract class HotCache
{
    /**
     * Постфикс для ключа данных в кеше
     */
    const POSTFIX_DATA = '-data';
    /**
     * Постфикс для лока при обновлении данных в кеше
     */
    const POSTFIX_REFRESH = '-refresh';
    /**
     * Постфикс для лока при создании данных в кеше
     */
    const POSTFIX_CREATE = '-create';

    /**
     * Промежуток для слипа при создании ключа в кеше
     */
    const SLEEP_DELAY = 100000;

    /**
     * Эта функция должна возвращать массив из трёх числовых значений. Для каждого
     * Ключа в кеше необходимы 3 значения - время, на которое мы кешируем данный,
     * горячее время и таймаут для записи данных.
     *
     * @param string $key
     * @return array
     */
    protected function getTTLSettings(string $key)
    {
        return [
            HotCacheTTL::DEFAULT_EXPIRE_TIME,
            HotCacheTTL::DEFAULT_HOT_TIME,
            HotCacheTTL::DEFAULT_SELECT_TIMEOUT
        ];
    }

    /**
     * @param string $key
     * @return HotCacheTTL
     */
    public function getTTL(string $key)
    {
        $key = preg_replace("#\W+#", "-", strtolower($key));
        $ttl = new HotCacheTTL();

        $ttlSettingsArray = $this->getTTLSettings($key);

        $ttl->setExpire(array_shift($ttlSettingsArray));
        $ttl->setHotTime(array_shift($ttlSettingsArray));
        $ttl->setselectTimeout(array_shift($ttlSettingsArray));
        return $ttl;
    }

    public function get(string $keyPrefix, array $keyOptions = [], HotCacheTTL $ttl, callable $getDataCallback)
    {
        $valueExpire = $ttl->getExpire();
        $valueHotTime = $ttl->getHotTime();
        $selectTimeOut = $ttl->getSelectTimeout();

        if (!$this->cacheEnabled()) {
            return call_user_func($getDataCallback);
        }

        $keyPrefix = $this->getKey($keyPrefix, $keyOptions);

        /**
         * пытаемся получить данные из кеша
         */
        $pack = $this->cacheGet($keyPrefix . self::POSTFIX_DATA);

        if ($pack) {
            list($data, $time) = $pack;
            /**
             * Если данные в кеше есть, но "горячее" время кончилось - пора обновить данные в кеше
             */
            if ($time + $valueHotTime < time()) {
                /**
                 * Получаем лок на обновление данных. Только 1 клиент обновляет данные, остальные забирают
                 * данные из кеша
                 */
                if ($this->cacheAdd($keyPrefix . self::POSTFIX_REFRESH, 1, $selectTimeOut)) {
                    $data = call_user_func($getDataCallback);
                    $this->cacheSet($keyPrefix . self::POSTFIX_DATA, array($data, time()), $valueExpire);
                    $this->cacheDelete($keyPrefix . self::POSTFIX_REFRESH);
                }
            }
            return $data;
        }

        /**
         * Если данных нет, только 1 клиент выбирает данные, остальные - ждут
         */
        if ($this->cacheAdd($keyPrefix . self::POSTFIX_CREATE, 1, $selectTimeOut)) {
            $data = call_user_func($getDataCallback);
            $this->cacheSet($keyPrefix . self::POSTFIX_DATA, array($data, $this->getTime()), $valueExpire);
            $this->cacheDelete($keyPrefix . self::POSTFIX_CREATE);
            return $data;
        }

        /**
         * Другие клиенты ждут появления данных в кеше
         */
        $time = time();
        while ($time + $selectTimeOut > time()) // waiting
        {
            usleep(self::SLEEP_DELAY);
            $pack = $this->cacheGet($keyPrefix . self::POSTFIX_DATA);
            if ($pack) {
                list($data, $time) = $pack;
                return $data;
            }
        }

        /**
         * Если ожидание завершилось ничем, забираем данные из хранилища
         */
        $data = call_user_func($getDataCallback);
        $this->cacheSet($keyPrefix . self::POSTFIX_DATA, array($data, time()), $valueExpire);
        return $data;
    }

    /**
     * @param $keyPrefix
     * @param array $keyOptions
     */
    public function drop(string $keyPrefix, array $keyOptions = [])
    {
        $keyPrefix = $this->getKey($keyPrefix, $keyOptions);
        $this->cacheDelete($keyPrefix . self::POSTFIX_DATA);
    }

    /**
     * удалить значение из кеша по ключу
     * @param string $key
     * @return bool
     */
    abstract protected function cacheDelete(string $key);

    /**
     * включено ли кеширование
     * @return bool
     */
    abstract protected function cacheEnabled();

    /**
     * @param string $key
     * @return mixed
     */
    abstract protected function cacheGet(string $key);

    /**
     * @param string $key
     * @param $value
     * @param int $expire
     * @return bool
     */
    abstract protected function cacheAdd(string $key, $value, int $expire = 0);

    /**
     * @param string $key
     * @param $value
     * @param int $expire
     * @return mixed
     */
    abstract protected function cacheSet(string $key, $value, int $expire = 0);

    /**
     * @param string $keyPrefix
     * @param array $keyOptions
     * @return string|string
     */
    protected function getKey(string $keyPrefix = '', array $keyOptions = [])
    {
        if (is_array($keyOptions)) {
            $this->sortByKeyRecursive($keyOptions);
            $keyPrefix .= "-" . md5(serialize($keyOptions));
        } else {
            if ($keyOptions) {
                $keyPrefix .= "-" . $keyOptions;
            }
        }
        return $keyPrefix;
    }

    /**
     * @param array $array
     * @param int $sortFlags
     * @return bool
     */
    private function sortByKeyRecursive(array &$array, int $sortFlags = SORT_REGULAR)
    {
        $isSorted = ksort($array, $sortFlags);
        foreach ($array as &$value) {
            if (is_array($value) && !empty($value)) {
                $isSorted = $this->sortByKeyRecursive($value, $sortFlags) && $isSorted;
            }
        }
        unset($value);
        return $isSorted;
    }
}