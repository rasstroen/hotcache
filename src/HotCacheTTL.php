<?php

namespace bee;

class HotCacheTTL
{
    /**
     * Время жизни кеша
     */
    const DEFAULT_EXPIRE_TIME = 1000;
    /**
     * Время, в которое не будет попыток переположить значение в кеш. По истечении этого времени один процесс будет
     * перезаписывать кеш, а остальные - брать данные из кеша
     */
    const DEFAULT_HOT_TIME = 900;
    /**
     * Время лока на запись в кеш. Только 1 процесс будет выбирать данные и сохранять их в кеш,
     * остальные процессы дождутся выполнения этого сохранения
     */
    const DEFAULT_SELECT_TIMEOUT = 1;

    /**
     * @var int
     */
    private $expire = self::DEFAULT_EXPIRE_TIME;
    /**
     * @var int
     */
    private $hotTime = self::DEFAULT_HOT_TIME;
    /**
     * @var
     */
    private $selectTimeout = self::DEFAULT_SELECT_TIMEOUT;

    /**
     * @param int $expire
     */
    public function setExpire(int $expire)
    {
        $this->expire = $expire;
    }

    /**
     * @return int
     */
    public function getExpire()
    {
        return $this->expire;
    }

    /**
     * @param int $hotTime
     */
    public function setHotTime(int $hotTime)
    {
        $this->hotTime = $hotTime;
    }

    /**
     * @return int
     */
    public function getHotTime()
    {
        return $this->hotTime;
    }

    /**
     * @param int $selectTime
     */
    public function setSelectTimeout(int $selectTimeout)
    {
        $this->selectTimeout = $selectTimeout;
    }

    /**
     * @return int
     */
    public function getSelectTimeout()
    {
        return $this->selectTimeout;
    }
}